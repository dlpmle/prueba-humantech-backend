<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    protected $fillable = [
        'turno',
        'estado'
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function peliculaTurno(){
        return $this->hasMany(\App\PeliculaTurno::class);
    }
}
