<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model

{
    protected $fillable = [
        'nombre',
        'fecha_publicacion',
        'estado',
        'ruta_imagen'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function peliculaTurno(){
        return $this->hasMany(\App\PeliculaTurno::class);
    }
}
