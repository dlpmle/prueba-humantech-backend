<?php

namespace App\Http\Controllers;

use App\Pelicula;
use Illuminate\Http\Request;

class PeliculaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peliculas = Pelicula::orderBy('id', 'desc')->get();
        return $peliculas;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['fecha_publicacion'] = $request->fpublicacion;
        $request['estado'] = 1;
        $request['ruta_imagen'] = 'imagenes/' . $request['nombre'];
        $pelicula = Pelicula::create($request->all());
        return response()->json([
                'status_code' => 200,
                'data' => 'se creo correctamente la pelicula ' . $pelicula->id
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pelicula $pelicula)
    {
        return $pelicula;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pelicula $pelicula)
    {
        return $pelicula;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pelicula $pelicula)
    {
        $pelicula->update($request->all());
        return response()->json([
            'status_code' => 200,
            'data' => 'se actualizo correctamente la pelicula ' . $pelicula->id
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pelicula $pelicula)
    {
        $pelicula_id = $pelicula->id;
        $pelicula = $pelicula->delete();
        return response()->json([
            'status_code' => 200,
            'data' => 'se elimino correctamente la pelicula ' . $pelicula_id
        ]);
    }
}
