<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeliculaTurno extends Model
{
    protected $fillable = [
      'pelicula_id',
      'turno_id'  
    ];

    public function turno(){
        return $this->belongsTo(\App\Turno::class);
    }

    public function pelicula(){
        return $this->belongsTo(\App\Pelicula::class);
    }
}
